<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Setting') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
{{--Style--}}
<!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    {{--    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/firstStyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/stylerespons.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    {{--Script--}}
    <script src="{{asset('js/bootstrap/jquery-3.1.0.min.js')}}"></script>
    {{--Toaster--}}
    <link rel="stylesheet" href="{{asset('toaster/jquery.toast.css')}}">
</head>
<body class="settingBody">
<div class="settingHeaderBar">
    <a class="arrowDiv" href="{{route('setting')}}" >
        <div class="arrowButton arrowSwitch" >
            <div class="arrow"></div>
        </div>
    </a>
</div>

<div>
    <div class="changePassDiv">
        <h4>Change Password</h4>
        @if(Session::has('message'))
            <p class="alert text-center {{ Session::get('alert-class', 'alert-danger') }}"  >{{ Session::get('message') }}</p>
        @endif
        <form action="{{route('changePass')}}" method="post" >
            @csrf
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control col-md-7">
            <label for="password">Password</label>
            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} col-md-7"
                   type="password" name="password"  required>
            <label for="password_confirmation">Password Confirm</label>
            <input id="password-confirm" type="password" class="form-control col-md-7"
                   name="password_confirmation"  required>
            <button class="btn btn-3 btn-3e btn-default" >Submit</button>

        </form>

            <p class="col-md-7 m-auto alert  {{ Session::get('alert-class', 'alert-danger') }}">Incorrect Password</p>

    </div>
</div>


<script src="{{asset('toaster/jquery.toast.js')}}" defer></script>
<script src="{{asset('js/command.js')}}"></script>
</body>
</html>
