<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Quick Add Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" id="aForm">@csrf
            <div class="modal-body">
                <h5 id="newTaskEdit"></h5>
                <textarea class="form-control"  name="newTaskComm" id="newTaskCom" cols="30" rows="10"></textarea>
                <small id="newTaskDataEdit"></small>
                <small id="newTaskDataTime"></small>
            </div>
            <div class="modal-footer">
                <input type="email"  name="sendEmail" class="form-control w-50">
                <span  id="send_btn" class="btn btn-3 btn-3e "> Send </span>

                <button type="button" class="btn btn-3 btn-3e" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-3 btn-3e" id="newTaskUp" >Save</button>
            </div></form>
        </div>
    </div>
</div>
