
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Quick Add Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">@csrf
                    <input type="text" name="newTask" id="new-task-value" class="form-control" placeholder="New task">
                    <input type='text' name="newTaskData" id='timePicker' class="form-control" data-large-mode="true"
                           data-min-year="2017" data-max-year="2029">
                    <span class="error_newTask alert alert-danger"></span>
                    <input type="text" name="newTaskTime" id="alarm" class="form-control"/>
                    <textarea name="newTaskComm" class="form-control" id="newTaskComm" cols="30" rows="10"
                              placeholder="Comment"></textarea>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-3 btn-3e" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-3 btn-3e" id="newTask">Save new task</button>
            </div>
        </div>
    </div>
</div>




{{--Mail Modal--}}

<div class="modal fade mt-4" id="mail_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send mail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post">@csrf
                <div class="modal-body">

                    <input type="email" id="send_mail" name="sendEmail" class="form-control  pl-3" placeholder="Send to E-Mail">


                </div>
                <div class="modal-footer">
                    <span id="send_btn2" class="btn btn-3 btn-3e ">Submit</span>
                </div>
            </form>
        </div>
    </div>
</div>
