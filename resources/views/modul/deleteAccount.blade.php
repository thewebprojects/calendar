<div class="modal fade" id="deleteAccountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Quick Add Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/deleteAccount" id="" method="post" class="p-2">@csrf
                <input type="password" class="form-control m-4 w-75 " name="password" placeholder="Your Password">
                <button class="btn btn-3 btn-3e float-right mr-2 " id="newTaskUp">Save</button>
                <button type="button" class="btn btn-3 btn-3e float-right mr-2" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
