@extends('layouts.app')

@section('content')
    <div class="form-body">

        <div class="row">
            <div class="img-holder">
                <div class="info-holder">
                    <img src="{{asset('image/graphic2.svg')}}" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Get more things done with Loggin platform.</h3>
                        <p>Access to the most powerfull tool in the jhhhhghghgentire design and web industry.</p>
                        <div class="page-links">
                            <a href="{{route('login')}}" class="active">Login</a><a href="{{route('register')}}">Register</a>
                        </div>
                        <form method="POST" action="{{ route('login') }}">  @csrf
                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email"
                                   name="email" placeholder="E-mail Address"
                                   value="{{ old('email') }}" required>
                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   type="password" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <div class="form-button">
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>

                                <button id="submit" type="submit" class="ibtn">{{ __('Login') }}</button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
