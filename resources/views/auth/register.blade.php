@extends('layouts.app')

@section('content')


    {{--Login Reg--}}

    <div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="info-holder">
                    <img src="{{asset('image/graphic2.svg')}}" alt="">
                </div>
            </div>
            <div class="row">
                <div class="img-holder">
                    <div class="bg"></div>
                    <div class="info-holder">
                        <img src="{{asset('image/graphic2.svg')}}" alt="">
                    </div>
                </div>
                <div class="form-holder">
                    <div class="form-content">
                        <div class="form-items">
                            <h3>Get more things done with Loggin platform.</h3>
                            <p>Access to the most powerfull tool in the entire design and web industry.</p>
                            <div class="page-links">
                                <a href="{{route('login')}}">Login</a><a href="{{route('register')}}"
                                                                         class="active">Register</a>
                            </div>
                            <form method="POST" action="{{ route('register') }}">@csrf
                                <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" type="text"
                                       name="name" value="{{ old('name') }}" placeholder="Full Name" required>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email"
                                       value="{{ old('email') }}" name="email" placeholder="E-mail Address" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       type="password" name="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" placeholder="Password confirm" required>

                                <div class="form-button">
                                    <button id="submit" type="submit" class="ibtn">{{ __('Register') }}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
