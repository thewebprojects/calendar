<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Setting') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
{{--Style--}}
<!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    {{--    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/firstStyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/stylerespons.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    {{--Script--}}
    <script src="{{asset('js/bootstrap/jquery-3.1.0.min.js')}}"></script>
    {{--Toaster--}}
@include('modul/deleteAccount')
</head>
<body class="settingBody">
<div class="settingHeaderBar  ">
    <a class="arrowDiv" href="{{route('inbox')}}">
        <div class="arrowButton arrowSwitch">
            <div class="arrow"></div>
        </div>
    </a>
</div>

<div class="settingBar">
    <div class="settingBarHead">
        <img src="
@if(!$user['img']== null)
        {{asset('userimg/user'.$user['id'].'/'.$user['img'])}}
        @else
        {{asset('img\loginImg.png')}}
        @endif
            " alt="">
        <h4>Jony Obroun</h4>
        <h6>Jony@gmail.com</h6>
    </div>
    <div>
        <a href="{{route('profile')}}" class="proflieButtonOne">
            <div class="settingBarBody ">
                Profile
            </div>
        </a>
    </div>
    <div>
        <a href="{{route('syncGoogle')}}" class="proflieButtonOne">
            <div class="settingBarFoot ">
                synchronizing whit the google
            </div>
        </a>
    </div>
    <div>
        <a href="{{route('passChange')}}" class="proflieButtonOne" >
            <div class="settingBarFoot ">
                Change password
            </div>
        </a>
    </div>
    <div>
        <a href="" class="proflieButtonOne" data-toggle="modal" data-target="#deleteAccountModal">
            <div class="settingBarFoot ">
                Delete Account
            </div>
        </a>
    </div>
</div>
@if(Session::has('message'))
    <p class="alert text-center  {{ Session::get('alert-class', 'alert-danger') }}" id="deleteError">{{ Session::get('message') }}</p>
@endif

</body>
</html>
