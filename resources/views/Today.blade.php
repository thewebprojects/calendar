@extends('layouts.app')

@section('content')
    <section class="sec">
        <div class="container">
            <div class="welcome">
                <h4>Today</h4>
                <p></p>

            </div>
            <div>
                <table class="table">
                    <thead>
                    <tr>

                        <th scope="col">Task Name</th>
                        <th scope="col">Data</th>
                        <th scope="col">Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($newtasks as $newtask)
                        <tr id="row_{{$newtask->id}}">

                            <td>{{substr($newtask->newTask, 0 ,10)}}</td>
                            <td>{{$newtask->newTaskData}}</td>
                            <td>{{$newtask->newTaskTime}}
                                <a href="" class="taskModul" data-id="{{$newtask->id}}" data-toggle="modal"
                                   data-target="#exampleModal1">
                                    <i class="far fa-comment-alt"></i>
                                </a>
                            </td>
                            <td>
                                <div class="dropdown ">
                                    <i class="fas fa-ellipsis-h dropdown_link"></i>
                                    <div class="taskMen">
                                        <a class="dropdown-item delete" href="#" data-id="{{$newtask->id}}">Delete</a>
                                        <ul>
                                            <li class="send_mail" data-toggle="modal" data-target="#mail_modal" data-id="{{$newtask->id}}">Send Mail</li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script>

    </script>


@endsection
