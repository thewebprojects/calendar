<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>My Calendar</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    {{--Style--}}
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
{{--    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/firstStyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datedropper.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/stylerespons.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/timedropper.css') }}" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="{{asset('css/login/iofrm-style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/login/iofrm-theme5.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    {{--Script--}}
    <script src="{{asset('js/bootstrap/jquery-3.1.0.min.js')}}"></script>
    <script src="{{asset('js/datedropper.js')}}"></script>
    <script src="{{asset('js/timedropper.js')}}"></script>


    {{--Toaster--}}
    <link rel="stylesheet" href="{{asset('toaster/jquery.toast.css')}}">



</head>
<body class="bg-light ">
    @include('modul/newproduct')
    @include('modul/productEdit')
        @auth
            <div class="container-fluid header">
                <button class="menu-button " id="open-button">Open Menu</button>
                <div class="search_box_place">
                    {{--<form class="searchbox">--}}
                        {{--<input type="search" placeholder="Search......" name="search" class="searchbox-input" onkeyup="buttonUp();" required>--}}
                        {{--<input type="submit" class="searchbox-submit" value="GO">--}}
                        {{--<span class="searchbox-icon"><img src="{{asset('img/search.svg')}}" alt="" width="50%"></span>--}}
                    {{--</form>--}}
                </div>
                <div class="top-right links">
                    <ul>
                        <li><i class="fas fa-plus" data-toggle="modal" data-target="#exampleModal"></i></li>
                        <li class="nav-item dropdown">

                            <i class="fas fa-bell notification_icon " data-toggle="dropdown"></i>

                            <div class="notification_icon">
                                <span class="not_position">{{count($tasks['forOneDay'])+count($tasks['forOnehours'])}}</span>
                            </div>
                            @if(count($tasks['forOneDay'])!= 0 || count($tasks['forOnehours']) != 0)
                            <div class="dropdown-menu dropdwn">
                                <h5>Notifications</h5>
                                @if(count($tasks['forOnehours']) != 0)
                                <h6>Time</h6>
                                @endif
                               @foreach($tasks['forOnehours'] as $task)
                                    <span href="{{action('FirstController@task')}}" >
                                        <h6>{{$task->newTask}}</h6>
                                       <i class="far fa-clock"></i><small>{{$task->newTaskData}} {{$task->newTaskTime}}</small>
                                    </span>
                                   @endforeach
                                @if(count($tasks['forOneDay']) != 0)
                                    <h6>Day</h6>
                                @endif
                                @foreach($tasks['forOneDay'] as $task)
                                    <span href="{{action('FirstController@task')}}" >
                                        <h6>{{$task->newTask}}</h6>
                                       <i class="far fa-clock"></i><small>{{$task->newTaskData}} {{$task->newTaskTime}}</small>
                                    </span>
                                @endforeach
                            </div>
                                @endif
                        </li>
                        {{--{{ Auth::user()->notifications }}--}}
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/setting">Settings</a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>

                    </ul>
                </div>
            </div>
            <div class="colapse colapse-close bg-light">
                <ul>
                    <li><i class="fas fa-archive"></i>
                        <a href="{{route('inbox')}}" class="colmen ">Inbox</a>
                    </li>
                    <li><i class="far fa-calendar"></i>
                        <a href="{{route('today')}}" class="colmen "> Today</a>
                    </li>
                    <li><i class="far fa-calendar-alt"></i>
                        <a href="{{route('week')}}" class="colmen ">Next 7 days</a>
                    </li>

                </ul>
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                </ul>
            </div>
        @endauth

    <script>
        $('#timePicker').dateDropper();
        $('#alarm').timeDropper();
    </script>
            @yield('content')

    <script src="{{asset('toaster/jquery.toast.js')}}" defer></script>
    <script src="{{asset('js/command.js')}}"></script>
</body>
</html>
