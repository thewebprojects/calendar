<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Setting') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
{{--Style--}}
<!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    {{--    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/firstStyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/stylerespons.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    {{--Script--}}
    <script src="{{asset('js/bootstrap/jquery-3.1.0.min.js')}}"></script>
    {{--Toaster--}}
    <link rel="stylesheet" href="{{asset('toaster/jquery.toast.css')}}">
</head>
<body class="settingBody">
<div class="settingHeaderBar">
    <a class="arrowDiv" href="{{route('setting')}}">
        <div class="arrowButton arrowSwitch">
            <div class="arrow"></div>
        </div>
    </a>
</div>
<form action="{{route('profileEdit')}}" method="post" enctype="multipart/form-data" id="editProfileForm">
    @csrf
    <div class="settingProfile">
        <div class="profileImage">
            <div class="divAvatar">
                <img src="
                @if(!$user['img']== null)
                {{asset('userimg/user'.$user['id'].'/'.$user['img'])}}
                @else
                {{asset('img\loginImg.png')}}
                @endif
                    " alt="" class="avatar">
            </div>
            <div class="imageInput">
                <label for="loginfile" class="loginFile"><img src="{{asset('image/photo-camera.svg')}}" alt=""></label>
                <input type="file" name="loginfile" id="loginfile">
            </div>
        </div>
        <div class="profileInfo">
            <ul>
                <li class="name">Name</li>
                <li class="surname">Surname</li>
                <li class="email">Email</li>
                <li class="age">Age</li>
                <li class="location">Location</li>
                <li class="sex">Gender</li>
                <li class="phone">Phone Number</li>

            </ul>

            <ul class="infoInput">
                <li class="nameInput">{{$user->name}}</li>
                <li class="surnameInput">{{$user->surname}}</li>
                <li class="emailInput">{{$user->email}}</li>
                <li class="ageInput">{{$user->age}}</li>
                <li class="locationInput">{{$user->location}}</li>
                <li class="sexInput">{{$user->gender}}</li>
                <li class="phoneInput">{{$user->phone}}</li>
            </ul>

            <span class="btn btn-3 btn-3e " id="EditProfile">Edit</span>

        </div>

        <div class="profileEdit">

            <input type="text" class="form-control col-md-6" name="name" value="{{$user->name}}">
            <input type="text" class="form-control col-md-6" name="surname" value="{{$user->surname}}">
            <input type="email" class="form-control col-md-6" name="email" value="{{$user->email}}">
            <input type="number" class="form-control col-md-6" name="age" value="{{$user->age}}">
            <input type="text" class="form-control col-md-6" name="location" value="{{$user->gender}}">
            <input type="text" class="form-control col-md-6" name="gender" value="{{$user->location}}">
            <input type="text" class="form-control col-md-6" name="phone" id="editPhone" value="{{$user->phone}}">


            <button type="button" class="btn btn-3 btn-3e editBtn" id="settingButton">Save</button>
            <span class="btn btn-3 btn-3e" id="EditProfileCancel">Cancel</span>

        </div>
    </div>
</form>
<script src="{{asset('toaster/jquery.toast.js')}}" defer></script>

<script src="{{asset('js/command.js')}}"></script>
</body>
</html>
