$(document).ready(function () {
    // Ajax CSRF
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //END AJAX CSRF
    //MENU CLICK TASK


    $(document).click(function () {
        $('.taskMen').hide();
    });
    $('.dropdown_link').click(function (e) {
        e.stopPropagation();
        $(this).next().toggle(200);
    });
    //END TASK MENU CLICK
    //UL LI MENU
    var x = 0;
    $('.men').on('click', function () {
        if (x == 0) {
            x = 1;
            $(this).find('ul').addClass('open');
            $(this).find('i').css('transform', 'rotate(90deg)');
            $('.colmen').addClass('animated', 'fadeInDown', 'delay-2s');
        } else {
            x = 0;
            $(this).find('ul').removeClass('open');
            $(this).find('i').css('transform', 'rotate(0deg)');
        }
    });
    //END UL LI MENU

    $('#newTask').on('click', function () {
        var val = $('#new-task-value').val();
        var time = $('#alarm').val();
        var valData = $('#timePicker').val();
        var newTaskComm = $('#newTaskComm').val();
        var data = {
            newTaskComm: newTaskComm,
            newTask: val,
            time: time,
            valData: valData
        };
        $.ajax({
            url: 'newtask',
            type: 'post',
            data: data,
            success: function (data) {
                if (data.error == true) {
                    $('.error_newTask').text(data.message);
                    $('.error_newTask').css('display', 'block');

                } else {
                    location.reload();
                }
            }

        })

    });

    $(".taskModul").on('click', function () {
        var id = $(this).data(id);
        $.ajax({
            url: 'productedit',
            type: 'post',
            dataType: 'json',
            data: {id: id},
            success: function (data) {
                $("#newTaskEdit").html(data.newTask);
                $("#newTaskDataEdit").text(data.newTaskData);
                $('#newTaskDataTime').text(data.newTaskTime);
                $('#newTaskCom').val(data.newTaskComm);
                $('#newTaskUp').attr('data-id', data.id);
                $('#send_btn').attr('data-id', data.id);
            }
        })
    });

    $("#newTaskUp").on('click', function () {
        var val = $("#newTaskCom").val();
        var id = $("#newTaskUp").data('id');
        $.ajax({
            url: 'productuplode',
            type: 'post',
            data: {
                id: id,
                val: val,
            },
            success: function (data) {
                location.reload();
            }
        });
    });

    //Delete Task
    $('.delete').on('click', function () {
        var id = $(this).data('id');
        var row_id = $('#row_' + id)
        $.ajax({
            url: 'delete',
            type: 'post',
            data: {id: id},
            success: function (data) {
                row_id.hide();
            }
        })
    });

    //Search
    $(document).ready(function () {
        var submitIcon = $('.searchbox-icon');
        var inputBox = $('.searchbox-input');
        var searchBox = $('.searchbox');
        var isOpen = false;
        submitIcon.click(function () {
            if (isOpen == false) {
                searchBox.addClass('searchbox-open');
                inputBox.focus();
                isOpen = true;
            } else {
                searchBox.removeClass('searchbox-open');
                inputBox.focusout();
                isOpen = false;
            }
        });
        submitIcon.mouseup(function () {
            return false;
        });
        searchBox.mouseup(function () {
            return false;
        });
        $(document).mouseup(function () {
            if (isOpen == true) {
                $('.searchbox-icon').css('display', 'block');
                submitIcon.click();
            }
        });
    });

    function buttonUp() {
        var inputVal = $('.searchbox-input').val();
        inputVal = $.trim(inputVal).length;
        if (inputVal !== 0) {
            $('.searchbox-icon').css('display', 'none');
        } else {
            $('.searchbox-input').val('');
            $('.searchbox-icon').css('display', 'block');
        }
    }

//    slidebar

    $('#open-button').on('click', function () {

        if ($(this).hasClass('menu-button-open')) {
            $(this).removeClass('menu-button-open');
            $('.colapse-close').removeClass('colapse-open');
        } else {

            $(this).addClass('menu-button-open');
            $('.colapse-close').addClass('colapse-open');
        }

    });


    $('#send_btn').on('click', function () {

        var email = $(this).prev('input').val();
        var id = $(this).data('id');
        if (email == '') {
            $.toast({
                heading: 'Error',
                loader: false,
                text: 'Fill the field',
                position: 'top-center',
                icon: 'error'
            });
        }
        $.ajax({
            url: 'send',
            type: 'post',
            data: {
                email: email,
                id: id,
            },
            success: function (data) {
                $.toast({
                    heading: 'Sand E-mail',
                    loader: false,
                    text: 'Your message has been sent',
                    position: 'bottom-center',
                    icon: 'success'
                });

            },
        })
    });
    $('#send_btn2').on('click', function () {
        var email = $('#send_mail').val();
        var id = $(this).data('id');

        if (email == '') {
            $.toast({
                heading: 'Error',
                loader: false,
                text: 'Fill the field',
                position: 'top-center',
                icon: 'error'
            });
        }


        $.ajax({
            url: 'send',
            type: 'post',
            data: {
                email: email,
                id: id,
            },
            success: function (data) {
                $('#mail_modal').modal('hide');
                $.toast({
                    heading: 'Sand E-mail',
                    loader: false,
                    text: 'Your message has been sent',
                    position: 'bottom-center',
                    icon: 'success'
                });
            },
        })
    });
    $('.send_mail').on('click', function () {

        var id = $(this).data(id);
        $.ajax({
            url: 'productedit',
            type: 'post',
            dataType: 'json',
            data: {id: id},
            success: function (data) {
                $('#send_btn2').attr('data-id', data.id);
            }
        })
    });

//    Proflie Edit
    $(document).on('click', '#EditProfile', function () {
        $(".profileEdit").eq(0).css('display', 'block');
        $(".imageInput").eq(0).css('display', 'block');
        $(".infoInput").eq(0).css('display', 'none');

    });
    $(document).on('click', '#EditProfileCancel', function () {
        $(".profileEdit").eq(0).css('display', 'none');
        $(".imageInput").eq(0).css('display', 'none');
        $(".infoInput").eq(0).css('display', 'block');



    });


    // Profile Edit

    // get File in input

    $('#settingButton').on('click', function () {
        var phoneval = $('#editPhone').val().length;
        var file_data = $('#loginfile').prop('files')[0];
        var form_data = new FormData($('#editProfileForm')[0]);
        form_data.append('file', file_data);
        if (phoneval > 16) {
            $.toast({
                heading: 'Error',
                loader: false,
                text: 'Phone number can\'t be more than 16 characters',
                position: 'top-center',
                icon: 'error',

            });
        } else {
            $.ajax({
                url: 'passChange/profileEdit',
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                data: form_data,
                success: function (data) {
                    $.toast({
                        heading: 'Profile',
                        loader: false,
                        text: 'Your profile is edited ',
                        position: 'bottom-center',
                        icon: 'success',
                    });
                     location.reload();
                }
            })
        }
    });

    $('.changePassDiv>p').css({
        'display':'none ',
 });

    $('.changePassDiv>form>input').on('change',function () {
        var changePass = $('.changePassDiv>form>input[name=password]').val();
        var changePassConfirm = $('.changePassDiv>form>input[name=password_confirmation]').val();
        if(changePass != changePassConfirm){
            $('.changePassDiv>form>button').css({
                'pointer-events':'none',
                'background-color':'#e7e7e7',
                'color':'black',
            });
            $('.changePassDiv>p').css({
                'display':'block ',

            });

        }else {
            $('.changePassDiv>form>button').css({
                'pointer-events':'auto',
                'background-color':'#3490dc',
                'color':'white',
            });
            $('.changePassDiv>p').css({
                'display':'none ',
            });
        }
    });



});
