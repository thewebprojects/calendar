<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class newTask extends Model
{
    protected $table = 'new_tasks';
    protected $fillable =[
      'newTask',
      'newTaskData',
      'newTaskTime',
      'newTaskComm',
        'user_id',
    ];
}
