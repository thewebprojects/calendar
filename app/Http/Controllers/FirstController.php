<?php

namespace App\Http\Controllers;

use App\GoogleClient;
use App\models\newTask;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Mail;
use Extention;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use \Validator;
use Carbon\Carbon;

class firstController extends Controller
{

    protected $client;

    public function login()
    {

        return view('Login');
    }

    public function registration(Request $request)
    {
        return view('Registration');
    }

    public function setting()
    {
        $user = Auth::user();
        return view('setting', compact('user'));
    }

    public function profile()
    {
        $user = Auth::user();
        return view('profile', compact('user'));
    }

    public function passChange()
    {
        return view('changePass');
    }

    public function welcome()
    {
        $tasks = User::notify(1, 2);
        return view('welcome', compact('tasks'));
    }

    public function inbox()
    {
        $tasks = User::notify(1, 2);
        $id = Auth::id();
        $newtasks = newTask::where('user_id', $id)->paginate(5);
        return view('Inbox', compact('newtasks', 'id', 'tasks'));
    }

    public function today()
    {
        $tasks = User::notify(1, 2);
        $todays = Carbon::today()->format('m/d/Y');
        $id = Auth::id();
        $newtasks = newTask::where('newTaskData', $todays)->paginate(5);


        return view('Today', compact('todays', 'newtasks', 'tasks'));
    }

    public function week()
    {
        $tasks = User::notify(1, 2);
        $todays = Carbon::today()->format('m/d/Y');
        $id = Auth::id();
        $newtasks = newTask::where('newTaskData', '>', $todays)->paginate(5);
        return view('Week', compact('todays', 'newtasks', 'tasks'));
    }

    public function productedit(Request $request)
    {
        $id = $request->input('id');
        $newtasks = newTask::where('id', $id['id'])->first();
        echo json_encode($newtasks);

    }

    public function edit(Request $request)
    {
        $val = $request->input('val');

        $id = $request->input('id');
        newTask::where('id', $id)->update(array('newTaskComm' => $val));;

    }

    public function newtask(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newTask' => 'required|min:3'
        ], [
            'newTask.required' => 'Fill in field',
            'newTask.min' => 'Min number should be 3'

        ]);
        if (!$validator->fails()) {
            $obj = [
                'error' => false,
            ];
            $id = Auth::user()->id;
            $user = User::where('id', $id)->first();
            $newTask = newTask::query()->create([
                'newTask' => $request->input('newTask'),
                'newTaskData' => $request->input('valData'),
                'newTaskTime' => $request->input('time'),
                'newTaskComm' => $request->input('newTaskComm'),
                'user_id' => $id,

            ]);
            if ($user->access_token != null) {
                $this->createNewcalendar();

                $summary = $request->input('newTask');
                $description = $request->input('newTaskComm');

                $time_zone = new \DateTimeZone("America/New_York");
                $startDate = $request->input('valData');
                $startTime = $request->input('time');
                $startDateTime = new \DateTime($startDate . ' ' . $startTime, $time_zone);
                $dataTime = $startDateTime->format(\DateTime::RFC3339);


                $event = new \Google_Service_Calendar_Event(array(
                    'summary' => $summary,
                    'location' => '800 Howard St., San Francisco, CA 94103',
                    'description' => $description,
                    'start' => array(
                        'dateTime' => $dataTime,
                        'timeZone' => 'America/Los_Angeles',
                    ),
                    'end' => array(
                        'dateTime' => $dataTime,
                        'timeZone' => 'America/Los_Angeles',
                    ),
                    'recurrence' => array(
                        'RRULE:FREQ=DAILY;COUNT=2'
                    ),
                    'reminders' => array(
                        'useDefault' => FALSE,
                        'overrides' => array(
                            array('method' => 'popup', 'minutes' => 24 * 60),
                            array('method' => 'popup', 'minutes' => 120),
                        ),
                    ),
                ));

                $service = new \Google_Service_Calendar($this->client);
                $calendarId = 'primary';
                $event = $service->events->insert($calendarId, $event);
dd( $event);
            }


        } else {
            $obj = [
                'error' => true,
                'message' => $validator->errors()->first()
            ];
        }


        return $obj;
    }

    public function delete(Request $request)
    {
        $delete = newTask::find($request->id);
        $delete->delete();
    }

    public function task()
    {
        $tasks = User::notify(1, 2);
        return view('Task', compact('tasks'));
    }

    public function sendEmail(Request $request)
    {
        $sendEmail = '';
        $sendEmail = $request->input('email');
        $id = $request->input('id');
        $email = Auth::user()->email;
        $data = newTask::where('id', $id)->first();
        Mail::send('emails.sendemail', $data->toArray(), function ($massage) use ($email, $sendEmail) {
            $massage->from($email, 'User:' . $email);
            $massage->to($sendEmail)->subject('Test');
        });

    }

    public function profileEdit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'editPhone' => 'max:16',
        ], [
            'editPhone.max' => 'Phone number can\'t be more than 16 characters'
        ]);
        $user['id'] = Auth::id();
        $data = $request->all();
        unset($data['_token']);

        if ($data['img']) {
            if (!$validator->fails()) {
                $user = User::where('id', $user['id'])->update($data);
            }
        } else {

            if (!$validator->fails()) {
                $user = User::where('id', $user['id'])->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'surname' => $data['surname'],
                    'age' => $data['age'],
                    'gender' => $data['gender'],
                    'location' => $data['location'],
                    'phone' => $data['phone'],
                ]);
            }
        }
    }

    public function createNewcalendar()
    {

        $userId = Auth::user()->id;
        $user = User::where('id', $userId)->first();
        $google = new GoogleClient();
        $this->client = $google->client;
        $this->client->setAccessToken($user->access_token);
        if ($this->client->isAccessTokenExpired()) {
            $this->client->fetchAccessTokenWithRefreshToken($user->access_token);
            $newAccessToken = $this->client->getAccessToken();
            $this->client->setAccessToken(\GuzzleHttp\json_encode($newAccessToken));
        }
        $this->client->authorize();
    }

    public function changePass(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        if ($user['email'] == $data['email']) {
            $user['password'] = Hash::make($data['password']);
            $user->save();
            return back();
        } else {
            Session::flash('message', 'Incorrect Email');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function deleteAccount(Request $request)
    {
        $user = Auth::id();
        $password = Auth::user();
        if (Hash::check($request->input('password'), $password['password'])) {
            User::where('id', $user)->delete();
            return redirect()->route('home');
        } else {
            Session::flash('message', 'Incorrect Password');
            return back();
        }
    }
}
