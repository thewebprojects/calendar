<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Oauth2;
use DateTime;
use DateTimeZone;
use App\GoogleClient;
use Illuminate\Support\Facades\Auth;
use Google_Client;

class GoogleClinetController extends Controller
{

    public function __construct() {

    }

    public function index(){

        $google = new GoogleClient();

        $auth_url = $google->client->createAuthUrl();
        header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
  die;
    }

    public function token() {

        $google = new GoogleClient();
        $authenticate =  $google->client->authenticate($_GET['code']);

        if(isset($authenticate['error'])){

            return redirect()->back();
        }
        $access_token['access_token'] = json_encode($google->client->getAccessToken());

        $id = Auth::user()->id;

        $user = new User();
        $user->where('id', $id)->update($access_token);

        return redirect()->back();
    }


}
