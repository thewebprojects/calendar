<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class GoogleCalendarController extends Controller
{

    protected $client;


    public function __construct() {
        $this->createNewcalendar();
    }
   public function createNewcalendar(){

        $userId = Auth::user()->id;
        $user = User::where('id', $userId)->first();
        $google = new GoogleClient();
        $this->client = $google->client;
        $this->client->setAccessToken($user->access_token);
        if ($this->client->isAccessTokenExpired()) {
            $this->client->fetchAccessTokenWithRefreshToken($user->access_token);
            $newAccessToken = $this->client->getAccessToken();
            $this->client->setAccessToken(\GuzzleHttp\json_encode($newAccessToken));

        }

        $this->client->authorize();
    }


}
