<?php

namespace App;

use App\models\newTask;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','surname','age', 'gender','location','phone','img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function notifications()
    {
        $user_id = Auth::id();
        return $this->hasOne('App\models\newTask', 'user_id')->where('user_id', $user_id);
    }

    public static function notify( $difference ,$hours)
    {
        $result = array('forOneDay'=>[],'forOnehours'=>[]);
        $now = Carbon::now();
        $id = Auth::id();
        $tasks = newTask::where('user_id', $id)->get();
        foreach ($tasks as $task) {
            $date = Carbon::parse($task->newTaskData . ' ' . $task->newTaskTime);
            $diff = $date->diffInDays($now);
            $diffHours = $date->diffInHours($now);
            if ($diff == $difference) {
                $result['forOneDay'][] = $task;
            }
            if ($diffHours == $hours) {
                $result['forOnehours'][] = $task;
            }

        }

        return $result;
    }
}
