<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

// Routs Function


Route::group(['middleware' => ['auth']], function () {
    Route::get('/inbox','FirstController@inbox')->name('inbox');
    Route::get('/today','FirstController@today')->name('today');
    Route::get('/week','FirstController@week')->name('week');
    Route::get('/welcome', 'FirstController@welcome')->name('welcome');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/setting','FirstController@setting')->name('setting');
    Route::get('/profile','FirstController@profile')->name('profile');
    Route::get('/passChange','FirstController@passChange')->name('passChange');
    Route::get('/sync','GoogleClinetController@index')->name('syncGoogle');
    Route::get('token', 'GoogleClinetController@token')->name('token');
    Route::get('/task', 'FirstController@task');
    Route::get('/notify', 'FirstController@notify');


    Route::post('newtask','FirstController@newtask');
    Route::post('productedit','FirstController@productedit');
    Route::post('productuplode','FirstController@edit');
    Route::post('delete','FirstController@delete');
    Route::post('send','FirstController@sendEmail');
    Route::post('passChange/profileEdit','FirstController@profileEdit')->name('profileEdit');
    Route::post('changePass','FirstController@changePass')->name('changePass');
    Route::post('/deleteAccount','FirstController@deleteAccount')->name('deleteAccount');
    Route::post('profileEdit','FirstController@profileEdit')->name('google');
    Route::post('changePass','FirstController@changePass')->name('changePass');


});

Auth::routes();
Route::get('/', function () {return redirect('/login');});
